package com.adelina.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SavedTranslationController {

	////////////// REST API
	@Autowired
	private CallRestService restService;

	@GetMapping("/translate")
	public ModelAndView GetTranslationForm(Model model) {
		ModelAndView mav = new ModelAndView("translate");
		TranslationData translation = new TranslationData();
		mav.addObject("translated", translation);
		return mav;
	}

	@PostMapping("/translate")
	public ModelAndView TranslaionSubmit(TranslationData translation) {
		ModelAndView mav = new ModelAndView("translate");
		TranslationData processedTranslation = restService.CallWordApi(translation.getText(), translation.getTo(),
				translation.getFrom());
		mav.addObject("translated", processedTranslation);
		return mav;
	}

	/////DB methods
	@Autowired
	private SavedTranslationService savedTranslationService;
	
	@RequestMapping(method = RequestMethod.POST, value = "/savedtranslation")
	public ModelAndView AddTranslation(TranslationData translation) {
		ModelAndView mav = new ModelAndView("saveditem");
		savedTranslationService.AddTranslation(translation);
		mav.addObject("translationsList", savedTranslationService.GetAllTranslations());
		return mav;
	}

	@RequestMapping(value = "/delete/{id}")
	public ModelAndView DeleteTranslation(@PathVariable Long id) {
		savedTranslationService.DeleteTranslation(id);
		ModelAndView mav = new ModelAndView("saveditem");
		mav.addObject("translationsList", savedTranslationService.GetAllTranslations());
		return mav;
	}

	
	@GetMapping(value = "/edit/{id}")
	public ModelAndView EditGetOneTranslation(@PathVariable Long id) {
		TranslationData translation = savedTranslationService.GetTranslation(id);
		ModelAndView mav = new ModelAndView("updateitem");
		mav.addObject("translated", translation);
		return mav;
	}

	
	@PostMapping(value = "/edittranslate/{id}")
	public ModelAndView EditOneTranslation(TranslationData translation, @PathVariable Long id) {
		ModelAndView mav = new ModelAndView("updateitem");
		TranslationData processedTranslation = restService.CallWordApi(translation.getText(), translation.getTo(),
				translation.getFrom());
		processedTranslation.setId(id);
		mav.addObject("translated", processedTranslation);
		return mav;
	}
	
	
	@PostMapping(value = "/edit/{id}")
	public ModelAndView EditPostOneTranslation(TranslationData translation, @PathVariable Long id) {
		translation.setId(id);
		savedTranslationService.UpdateTranslation(translation);
		ModelAndView mav = new ModelAndView("saveditem");
		mav.addObject("translationsList", savedTranslationService.GetAllTranslations());
		return mav;
	}
	
	
	//////////// other methods
	@RequestMapping("/savedtranslation")
	public String GetAllTranslations(Model model) {
		model.addAttribute("translations", savedTranslationService.GetAllTranslations());
		// return savedTranslationService.GetAllTranslations();
		return "saveditem";
	}

	@RequestMapping("/savedtranslation/{id}")
	public TranslationData GetTranslation(@PathVariable Long id, Model model) {
		return savedTranslationService.GetTranslation(id);
	}

	

	@RequestMapping(method = RequestMethod.PUT, value = "/savedtranslation/{id}")
	public ModelAndView UpdateTranslation(@RequestBody TranslationData translation, @PathVariable Long id) {
		translation.setId(id);
		savedTranslationService.UpdateTranslation(translation);
		
		ModelAndView mav = new ModelAndView("saveditem");
		mav.addObject("translationsList", savedTranslationService.GetAllTranslations());
		return mav;
	}


}
