package com.adelina.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class CallRestService {

	public TranslationData CallWordApi(String text, String to, String from) {
		RestTemplate restTemplate = new RestTemplate();
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://www.transltr.org/api/translate")
		        .queryParam("text", text)
		        .queryParam("to", to)
		        .queryParam("from", from);

		TranslationData translation = restTemplate.getForObject(builder.build().encode().toUri(), TranslationData.class);
		return translation;
	}
}
