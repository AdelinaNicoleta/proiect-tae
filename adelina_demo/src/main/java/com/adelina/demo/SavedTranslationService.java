package com.adelina.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SavedTranslationService {

	@Autowired
	private TranslationRepository translationRepository;
	
	public List<TranslationData> GetAllTranslations(){
		List<TranslationData> translations = new ArrayList<>();
		translationRepository.findAll()
		.forEach(translations::add);
		return translations;
	}
	
	public TranslationData GetTranslation(Long id){
		return translationRepository.findOne(id);
	}
	
	public void AddTranslation(TranslationData translation){
		translationRepository.save(translation);
	}
	
	public void UpdateTranslation(TranslationData translation){
		translationRepository.save(translation);
	}
	
	public void DeleteTranslation(Long id){
		translationRepository.delete(id);
	}
}
