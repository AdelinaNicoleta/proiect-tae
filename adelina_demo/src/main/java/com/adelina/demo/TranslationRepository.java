package com.adelina.demo;

import org.springframework.data.repository.CrudRepository;

public interface TranslationRepository extends CrudRepository<TranslationData, Long> {
}
